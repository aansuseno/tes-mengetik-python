import random
import tkinter as tk
from tkinter import ttk
from threading import Timer

# variable
kataJadi = []
fontSize = 20
banyakKataUser = 1
userArray = []
kataUser = ''
lamaNgetik = 60
root = tk.Tk()
root.title('TeS MeNgEtIk')
root.columnconfigure(0, weight=4)
frame1 = ttk.Frame(root)
frame2 = ttk.Frame(root)
masuk = ttk.Entry(frame1,width=33,font=('monospace', fontSize))
teks = ttk.Label(frame1,width=33,font=('monospace', fontSize))
pesan1 = ttk.Label(frame2,width=33,font=('monospace', fontSize))
pesan2 = ttk.Label(frame2,width=33,font=('monospace', fontSize))
pesan3 = ttk.Label(frame2,width=33,font=('monospace', fontSize))
frame3 = ttk.Frame(root)
frame3.columnconfigure(0, weight=1)
frame3.columnconfigure(1, weight=1)
frame3.grid(row=1,column=0)
pilih = tk.StringVar()
pilihMudah = ttk.Radiobutton(frame3, text='Mudah', value='mudah', variable=pilih)
pilihMudah.grid(row=0,column=0, padx=5, pady=5)
pilihSusah = ttk.Radiobutton(frame3, text='Susah', value='susah', variable=pilih)
pilihSusah.grid(row=0,column=1, padx=5, pady=5)

def bacaFile():
	global kataArray,banyakKata
	levelBaru = pilih.get()
	if (levelBaru == ''):
		levelBaru = 'mudah'
	namaFile = ''
	if (levelBaru == 'mudah'):
		namaFile = 'mudah.txt'
	elif (levelBaru == 'susah'):
		namaFile = 'kata.txt'
	kataList = open(namaFile, 'r')
	kataArray = kataList.readline().split('_')
	banyakKata = len(kataArray)

def acakKata():
	global kataJadi, banyakKata, kataArray, kataSekarang, kataString
	for i in range(100):
		kataJadi.insert(i, kataArray[random.randrange(0, banyakKata)])
	kataString = ' '.join(kataJadi)
	kataSekarang = kataString.split(' ')

def ngetik(event):
	global banyakKataUser, kataSekarang, jalan, kataUser, userArray
	if (jalan == True):
		jalan = False
		mulaiWaktu()
	kataUser = masuk.get()
	userArray = kataUser.split(' ')
	if (banyakKataUser < len(userArray)):
		banyakKataUser = len(userArray)
		kataSekarang.pop(0)
		teks['text'] = ' '.join(kataSekarang)

def berhenti():
	global kataJadi
	kataBenar = 0
	for i in range(len(userArray)):
		if (userArray[i] == kataJadi[i]):
			kataBenar+=1
	frame1.grid_forget()
	frame2.grid(column=0, row=0)
	pesan1['text']=f'Jumlah Karakter = {len(" ".join(userArray))}'
	pesan1.grid(column=0, row=0, padx=5, pady=5)
	pesan3['text']=f'Kata diketik = {len(userArray)}'
	pesan3.grid(column=0, row=1, padx=5, pady=5)
	pesan2['text'] = f'Kata benar = {kataBenar}'
	pesan2.grid(column=0, row=2, padx=5, pady=5)

t = Timer(lamaNgetik, berhenti)

def mulaiWaktu():
	global t
	t.start()

def mulai():
	global banyakKataUser,t,jalan,kataString
	t = Timer(lamaNgetik, berhenti)
	banyakKataUser = 1
	bacaFile()
	acakKata()
	frame2.grid_forget()
	jalan = True
	frame1.grid(row=0, column=0)
	teks['text'] = kataString[0:33]
	teks.grid(column=0, row=0, padx=5, pady=5)
	masuk.focus()
	masuk.grid(column=0, row=1, padx=5, pady=5)
	masuk.bind('<KeyPress>', ngetik)
	masuk.delete(0, len(masuk.get()))

def replayProses():
	global t
	t.cancel()
	mulai()

mulai()

tombolRefresh = ttk.Button(root,width=33,text='Mulai Ulang',command=replayProses)
tombolRefresh.grid(row=2, column=0, pady=10, padx=10)
root.mainloop()